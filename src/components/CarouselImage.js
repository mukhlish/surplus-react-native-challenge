import React from 'react';
import {StyleSheet, View, ActivityIndicator, Image} from 'react-native';

export const CarouselImage = ({index: _index}) => {
  const index = (_index || 0) + 123;
  return (
    <View style={styles.container}>
      <ActivityIndicator size="small" />
      <Image
        key={index}
        style={styles.image}
        source={{uri: `https://picsum.photos/id/${index}/400/300`}}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 8,
    overflow: 'hidden',
  },
  image: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});
