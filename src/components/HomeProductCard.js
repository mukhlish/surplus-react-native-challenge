import React from 'react';
import {
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Text,
  StyleSheet,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';

export default function HomeProductCard({index, item, type = 'product'}) {
  if (type === 'product') {
    return (
      <TouchableOpacity key={`product-${index}`} style={styles.container}>
        <ImageBackground
          source={{uri: item?.image}}
          style={styles.productImage}
          resizeMethod="scale"
          resizeMode="cover">
          <View style={styles.logoImageWrapper}>
            <Image
              source={{uri: item?.image}}
              style={styles.logoImage}
              resizeMethod="scale"
              resizeMode="contain"
            />
          </View>
          <View style={styles.distanceWrapper}>
            <Text>0.1 Km</Text>
          </View>
        </ImageBackground>

        <View style={{marginTop: 36, paddingHorizontal: 8}}>
          <Text style={{fontSize: 12, color: 'red', marginBottom: 8}}>
            Hampir habis!
          </Text>
          <Text
            style={{fontSize: 16, fontWeight: '600', marginBottom: 8}}
            numberOfLines={2}
            ellipsizeMode="tail">
            {item?.title}
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: '#4A4A4A',
              opacity: 0.5,
              marginBottom: 8,
            }}>
            Ambil hari ini, 09.00-20.00
          </Text>
          <View style={{flexDirection: 'row', marginBottom: 8}}>
            <Text
              style={{
                fontSize: 14,
                color: 'red',
                backgroundColor: 'rgba(255,0,0,0.1)',
                paddingHorizontal: 4,
                borderRadius: 8,
                marginRight: 6,
              }}>
              50%
            </Text>
            <Text
              style={{
                fontSize: 13,
                color: '#4A4A4A',
                opacity: 0.5,
                textDecorationLine: 'line-through',
                marginRight: 6,
              }}>
              ${item?.price * 2}
            </Text>
            <Text
              style={{
                fontSize: 14,
                color: '#4A4A4A',
                fontWeight: '600',
              }}>
              ${item?.price}
            </Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                fontSize: 12,
                color: '#4A4A4A',
                opacity: 0.5,
              }}>
              ⭐ {item?.rating.rate} | {item?.rating.count} terjual
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  } else {
    return (
      <TouchableOpacity
        style={[
          styles.container,
          {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            paddingHorizontal: 8,
          },
        ]}>
        <Text
          style={{flex: 1, fontSize: 18, color: '#3F9E93', fontWeight: '800'}}>
          Lihat semua produk
        </Text>
        <Feather name="arrow-right-circle" size={28} color="#3F9E93" />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    height: 380,
    width: 180,
    marginRight: 8,
    overflow: 'hidden',
    borderRadius: 16,
    borderWidth: 1,
    borderColor: '#F1F1F1',
  },
  productImage: {
    width: 180,
    height: 180,
    paddingHorizontal: 12,
    paddingVertical: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  logoImageWrapper: {
    width: 42,
    height: 42,
    borderRadius: 21,
    backgroundColor: '#F1F1F1',
    overflow: 'hidden',
    marginBottom: -28,
  },
  logoImage: {
    width: 42,
    height: 42,
    overflow: 'hidden',
    opacity: 0.7,
  },
  distanceWrapper: {
    backgroundColor: '#F1F1F1',
    paddingHorizontal: 4,
    paddingVertical: 2,
  },
});
