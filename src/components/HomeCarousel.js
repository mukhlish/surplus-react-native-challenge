import * as React from 'react';
import {Dimensions, StyleSheet, View} from 'react-native';
import Carousel from 'react-native-reanimated-carousel';
import {useSharedValue} from 'react-native-reanimated';

import {CarouselImage} from './CarouselImage';
import CarouselIndicator from './CarouselIndicator';

const WINDOW_WIDTH = Dimensions.get('window').width;
const colors = [
  '#3F9E93',
  '#3F9E93',
  '#3F9E93',
  '#3F9E93',
  '#3F9E93',
  '#3F9E93',
];

function HomeCarousel() {
  const progressValue = useSharedValue(0);

  return (
    <View>
      <Carousel
        loop
        width={WINDOW_WIDTH}
        height={WINDOW_WIDTH * 0.51}
        autoPlay={true}
        data={[...new Array(6).keys()]}
        scrollAnimationDuration={2000}
        mode="parallax"
        snapEnabled
        pagingEnabled
        on
        modeConfig={{
          parallaxScrollingScale: 0.9,
          parallaxScrollingOffset: 50,
        }}
        onProgressChange={(_, absoluteProgress) => {
          progressValue.value = absoluteProgress;
        }}
        renderItem={({index}) => (
          <View style={styles.itemContainer}>
            <CarouselImage index={index} />
          </View>
        )}
      />
      {!!progressValue && (
        <View style={styles.indicatorContainer}>
          {colors.map((backgroundColor, index) => {
            return (
              <CarouselIndicator
                backgroundColor={backgroundColor}
                animValue={progressValue}
                index={index}
                key={index}
                length={colors.length}
              />
            );
          })}
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    flex: 1,
  },
  indicatorContainer: {
    flexDirection: 'row',
    // justifyContent: 'space-between',
    // width: 100,
    paddingHorizontal: 20,
    marginBottom: 32,
  },
});

export default HomeCarousel;
