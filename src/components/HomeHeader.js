import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Dimensions,
  Platform,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';

export default function HomeHeader({navigation}) {
  return (
    <View style={styles.headerContainer}>
      <View style={styles.locationContainer}>
        <TouchableOpacity
          style={styles.locationWrapper}
          onPress={() => navigation.navigate('DummyLocation')}>
          <View style={styles.locationButtonWrapper}>
            <Text style={styles.locationButtonText}>Lokasi kamu</Text>
            <Feather name="chevron-down" size={12} color="#FFFFFF" />
          </View>
          <Text
            style={styles.locationDetailText}
            numberOfLines={1}
            ellipsizeMode="tail">
            Jl Gudang Peluru Timur Blok H no. 201, Kebon Baru, Tebet, Kota
            Jakarta Selatan, DKI Jakarta, Indonesia
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.headerIconButton}
          onPress={() => navigation.navigate('DummyFavorite')}>
          <Feather name="heart" size={28} color="#FFFFFF" />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.headerIconButton}
          onPress={() => navigation.navigate('DummyCart')}>
          <Feather name="shopping-cart" size={28} color="#FFFFFF" />
        </TouchableOpacity>
      </View>

      <View style={styles.searchContainer}>
        <Text style={styles.greetingText}>Hi, Surplus!</Text>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.searchInputWrapper}
          onPress={() => navigation.navigate('DummySearch')}>
          <TextInput
            editable={false}
            style={styles.searchInputText}
            placeholder="Mau selamatkan makanan apa hari ini?"
            placeholderTextColor={'#8E8E8E'}
          />
          <Feather
            style={styles.searchIcon}
            name="search"
            size={28}
            color="#C7C7C7"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: '#3F9E93',
    paddingHorizontal: 20,
    paddingTop: Platform.OS === 'ios' ? 46 : 16,
    width: Dimensions.get('screen').width,
    borderBottomLeftRadius: 36,
    borderBottomRightRadius: 36,
    marginBottom: 36,
  },
  locationContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  locationWrapper: {
    width: '70%',
  },
  locationButtonWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 4,
  },
  locationButtonText: {
    fontSize: 12,
    color: '#FFFFFF',
    marginRight: 4,
  },
  locationDetailText: {
    fontSize: 16,
    color: '#FFFFFF',
    fontWeight: '600',
  },
  headerIconButton: {
    paddingHorizontal: 8,
  },
  searchContainer: {
    bottom: -20,
  },
  greetingText: {
    fontSize: 24,
    color: '#FFFFFF',
    fontWeight: '600',
    marginBottom: 8,
  },
  searchInputWrapper: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: '#F1F1F1',
    borderRadius: 8,
    height: 48,
  },
  searchInputText: {
    flex: 1,
    height: 48,
    paddingHorizontal: 16,
  },
  searchIcon: {
    paddingHorizontal: 16,
  },
});
