import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';

export default function HomeShopCard({index, item, type = 'shop'}) {
  if (type === 'shop') {
    return (
      <TouchableOpacity key={`shop ${index}`} style={styles.container}>
        <Image
          source={{uri: item?.image}}
          style={styles.image}
          resizeMethod="scale"
          resizeMode="cover"
        />
        <Text style={styles.name} numberOfLines={2} ellipsizeMode="tail">
          {item?.title}
        </Text>
      </TouchableOpacity>
    );
  } else {
    return (
      <TouchableOpacity
        style={[
          styles.container,
          {
            flexDirection: 'column',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            padding: 8,
            borderWidth: 1,
            borderColor: '#F1F1F1',
            borderRadius: 16,
          },
        ]}>
        <Text
          style={{flex: 1, fontSize: 18, color: '#3F9E93', fontWeight: '800'}}>
          Lihat semua toko
        </Text>
        <Feather name="arrow-right-circle" size={28} color="#3F9E93" />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: 88,
    marginRight: 8,
  },
  image: {
    height: 88,
    width: 88,
    borderRadius: 16,
    marginBottom: 8,
  },
  name: {
    fontSize: 14,
    color: '#4A4A4A',
    textAlign: 'center',
  },
});
