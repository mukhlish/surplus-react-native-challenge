import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Splash from '../screens/Splash';
import Welcome from '../screens/Welcome';
import Register from '../screens/Register';
import Login from '../screens/Login';
import Home from '../screens/Home';
import DummyLocation from '../screens/DummyLocation';
import DummyFavorite from '../screens/DummyFavorite';
import DummyCart from '../screens/DummyCart';
import DummySearch from '../screens/DummySearch';

const Stack = createNativeStackNavigator();

export default function RootStackNavigator() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="Welcome" component={Welcome} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="DummyLocation" component={DummyLocation} />
      <Stack.Screen name="DummyFavorite" component={DummyFavorite} />
      <Stack.Screen name="DummyCart" component={DummyCart} />
      <Stack.Screen name="DummySearch" component={DummySearch} />
    </Stack.Navigator>
  );
}
