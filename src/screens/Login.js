import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as yup from 'yup';
import {Formik} from 'formik';

export default function Login() {
  const navigation = useNavigation();
  const [showPassword, setShowPassword] = useState(false);

  return (
    <KeyboardAvoidingView style={styles.container} behavior="height">
      <ScrollView contentContainerStyle={styles.container}>
        <Image
          source={require('../assets/Welcome-Screen-Back.png')}
          resizeMethod="scale"
          resizeMode="cover"
          style={styles.backgroundImage}
        />
        <View style={styles.contentContainer}>
          <View style={styles.contentTextWrapper}>
            <Text style={styles.contentTitleText}>Masuk</Text>
            <Text style={styles.contentSubTitleText}>
              Pastikan kamu sudah pernah{'\n'}membuat akun Surplus
            </Text>
          </View>
          <Formik
            initialValues={{
              email: '',
              password: '',
            }}
            onSubmit={values => {
              console.log('submit: ', values);
              navigation.navigate('Home');
            }}
            validationSchema={yup.object().shape({
              email: yup
                .string()
                .email('Masukkan email yang valid')
                .required('Email harus diisi'),
              password: yup
                .string()
                .min(4, 'Password harus lebih dari 4 karakter')
                .required('Password harus diisi'),
            })}>
            {({
              values,
              handleChange,
              errors,
              setFieldTouched,
              touched,
              isValid,
              handleSubmit,
            }) => (
              <View style={styles.formContainer}>
                <Text style={styles.labelText}>E-mail</Text>
                <View
                  style={styles.textInputContainer(
                    touched.email && errors.email,
                    values.email,
                  )}>
                  <TextInput
                    style={styles.textInput}
                    placeholder="Alamat email kamu"
                    placeholderTextColor={'#8E8E8E'}
                    value={values.email}
                    onChangeText={handleChange('email')}
                    onBlur={() => setFieldTouched('email')}
                  />
                </View>
                {touched.email && errors.email && (
                  <Text style={styles.errorText}>{errors.email}</Text>
                )}

                <Text style={styles.labelText}>Kata sandi</Text>
                <View
                  style={styles.textInputContainer(
                    touched.password && errors.password,
                    values.password,
                  )}>
                  <TextInput
                    style={styles.textInput}
                    placeholder="Masukkan kata sandi"
                    placeholderTextColor={'#8E8E8E'}
                    value={values.password}
                    onChangeText={handleChange('password')}
                    onBlur={() => setFieldTouched('password')}
                    secureTextEntry={!showPassword}
                  />
                  <TouchableOpacity
                    onPress={() => setShowPassword(prev => !prev)}>
                    <Ionicons
                      style={styles.inputIcon}
                      name={!showPassword ? 'eye-outline' : 'eye-off-outline'}
                      size={30}
                      color="#8E8E8E"
                    />
                  </TouchableOpacity>
                </View>
                {touched.password && errors.password && (
                  <Text style={styles.errorText}>{errors.password}</Text>
                )}
                <TouchableOpacity style={styles.forgotWrapper}>
                  <Text style={styles.forgotText}>Lupa kata sandi?</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  disabled={errors.email && errors.password && !isValid}
                  style={styles.mainButton(values.email && values.password)}
                  onPress={handleSubmit}>
                  <Text
                    style={styles.mainButtonText(
                      values.email && values.password,
                    )}>
                    Masuk
                  </Text>
                </TouchableOpacity>

                <View style={styles.separatorContainer}>
                  <View style={styles.separatorLine} />
                  <Text style={styles.separatorText}>Atau</Text>
                </View>

                <View style={styles.socialContainer}>
                  <TouchableOpacity style={styles.socialButtonContainer}>
                    <Image
                      source={require('../assets/google.png')}
                      style={styles.socialIcon}
                      resizeMethod="resize"
                      resizeMode="contain"
                    />
                    <Text style={styles.socialButtonText}>Google</Text>
                  </TouchableOpacity>
                </View>

                <View style={styles.otherActionWrapper}>
                  <Text style={styles.otherActionText}>
                    Belum punya akun?{' '}
                    <TouchableWithoutFeedback
                      onPress={() => navigation.navigate('Register')}>
                      <Text style={styles.otherLinkText}>Yuk daftar</Text>
                    </TouchableWithoutFeedback>
                  </Text>
                </View>
              </View>
            )}
          </Formik>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: '#3F9E93',
  },
  backgroundImage: {
    width: Dimensions.get('screen').width,
    height: 220,
    backgroundColor: '#3F9E93',
  },
  contentContainer: {
    flex: 1,
    marginTop: -112,
  },
  contentTextWrapper: {
    paddingHorizontal: 16,
  },
  contentTitleText: {
    fontSize: 32,
    fontWeight: '600',
    color: '#FFFFFF',
    marginBottom: 4,
  },
  contentSubTitleText: {
    fontSize: 12,
    color: '#FFFFFF',
    marginBottom: 12,
  },
  formContainer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    borderTopRightRadius: 32,
    borderTopLeftRadius: 32,
    paddingTop: 12,
    paddingBottom: 80,
    paddingHorizontal: 20,
  },
  textInputContainer: (error, filled) => ({
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: error ? '#FF0D10' : filled ? '#3F9E93' : '#F1F1F1',
    borderRadius: 8,
    height: 48,
  }),
  labelText: {
    fontSize: 14,
    color: '#4A4A4A',
    fontWeight: '800',
    marginBottom: 8,
    marginTop: 20,
  },
  textInput: {
    flex: 1,
    height: 48,
    paddingHorizontal: 16,
  },
  inputIcon: {
    paddingHorizontal: 16,
  },
  errorText: {
    fontSize: 12,
    color: '#FF0D10',
  },
  forgotWrapper: {
    alignSelf: 'flex-end',
    marginBottom: 24,
  },
  forgotText: {
    fontSize: 16,
    color: '#8E8E8E',
    textDecorationLine: 'underline',
  },
  mainButton: filled => ({
    backgroundColor: filled ? '#3F9E93' : '#F1F1F1',
    paddingVertical: 12,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    borderWidth: 2,
    borderColor: filled ? '#3F9E93' : '#F1F1F1',
    marginVertical: 14,
  }),
  mainButtonText: filled => ({
    fontSize: 18,
    fontWeight: '600',
    color: filled ? '#FFFFFF' : '#C7C7C7',
  }),
  separatorContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 40,
  },
  separatorLine: {
    width: '100%',
    height: 1,
    backgroundColor: '#F1F1F1',
  },
  separatorText: {
    position: 'absolute',
    fontSize: 16,
    color: '#C7C7C7',
    paddingHorizontal: 24,
    backgroundColor: '#FFFFFF',
  },
  socialContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 36,
  },
  socialButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F1F1F1',
    paddingVertical: 20,
    paddingHorizontal: 40,
    borderRadius: 40,
  },
  socialIcon: {
    height: 14,
    width: 14,
    marginRight: 12,
  },
  socialButtonText: {
    color: '#4A4A4A',
    fontSize: 14,
    fontWeight: '700',
  },
  termText: {
    fontSize: 14,
    color: '#8E8E8E',
    textAlign: 'center',
  },
  linkText: {
    color: '#F7C050',
  },
  otherActionWrapper: {
    marginTop: 16,
    alignItems: 'center',
  },
  otherActionText: {
    fontSize: 16,
    color: '#4A4A4A',
  },
  otherLinkText: {
    color: '#3F9E93',
    fontWeight: '800',
    textDecorationLine: 'underline',
  },
});
