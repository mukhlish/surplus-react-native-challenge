import React, {useEffect} from 'react';
import {Dimensions, Image, SafeAreaView, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';

export default function Splash() {
  const navigation = useNavigation();

  useEffect(() => {
    // setTimeout below just simulation for loading assets
    setTimeout(() => {
      navigation.navigate('Welcome');
    }, 2000);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <Image
        source={require('../assets/Surplus-Logo.webp')}
        resizeMethod="scale"
        resizeMode="contain"
        style={styles.logo}
      />
      <Image
        source={require('../assets/Certified-B-Corp-Logo.webp')}
        resizeMethod="scale"
        resizeMode="contain"
        style={styles.cert}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: Dimensions.get('window').width / 2,
  },
  cert: {
    position: 'absolute',
    bottom: -80,
    width: Dimensions.get('window').width / 3,
  },
});
