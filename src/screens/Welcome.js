import React from 'react';
import {
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  View,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Linking,
  Platform,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

export default function Welcome() {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={styles.container}>
      <Image
        source={require('../assets/Welcome-Screen-Back.png')}
        resizeMethod="scale"
        resizeMode="cover"
        style={styles.backgroundImage}
      />

      <View style={styles.modalContainer}>
        <View style={styles.titleContainer}>
          <Text style={styles.titleText}>Selamat datang di Surplus</Text>
          <Text style={styles.subTitleText}>
            Selamatkan makanan berlebih di aplikasi Surplus agar tidak terbuang
            sia-sia
          </Text>
        </View>

        <View>
          <TouchableOpacity
            style={styles.mainButton}
            onPress={() => navigation.navigate('Register')}>
            <Text style={styles.mainButtonText}>Daftar</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.secondaryButton}
            onPress={() => navigation.navigate('Login')}>
            <Text style={styles.secondaryButtonText}>
              Sudah punya akun? Masuk
            </Text>
          </TouchableOpacity>
        </View>

        <View>
          <Text style={styles.termText}>
            Dengan daftar atau masuk, Anda menerima
            <TouchableWithoutFeedback
              onPress={() =>
                Linking.openURL(
                  'https://www.surplus.id/syarat-ketentuan-kostumer',
                )
              }>
              <Text style={styles.linkText}> syarat dan ketentuan </Text>
            </TouchableWithoutFeedback>
            serta
            <TouchableWithoutFeedback
              onPress={() =>
                Linking.openURL(
                  'https://www.surplus.id/kebijakan-privasi-kostumer',
                )
              }>
              <Text style={styles.linkText}> kebijakan privasi</Text>
            </TouchableWithoutFeedback>
          </Text>
        </View>
      </View>

      <TouchableOpacity
        style={styles.floatingButton}
        onPress={() => navigation.navigate('Home')}>
        <Text style={styles.floatingButtonText}>Lewati</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  backgroundImage: {
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height / 2,
  },
  modalContainer: {
    flex: 1,
    minHeight: 345,
    marginTop: -32,
    paddingBottom: 32,
    backgroundColor: '#FFFFFF',
    borderTopRightRadius: 32,
    borderTopLeftRadius: 32,
    paddingVertical: 32,
    paddingHorizontal: 16,
    justifyContent: 'space-evenly',
  },
  titleContainer: {},
  titleText: {
    fontSize: 24,
    fontWeight: '800',
    color: '#4A4A4A',
    marginBottom: 10,
    textAlign: 'center',
  },
  subTitleText: {
    fontSize: 16,
    color: '#8E8E8E',
    marginBottom: 24,
    textAlign: 'center',
  },
  mainButton: {
    backgroundColor: '#3F9E93',
    paddingVertical: 12,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    borderWidth: 2,
    borderColor: '#3F9E93',
    marginBottom: 14,
  },
  mainButtonText: {
    fontSize: 18,
    fontWeight: '600',
    color: '#FFFFFF',
  },
  secondaryButton: {
    backgroundColor: '#FFFFFF',
    paddingVertical: 12,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    borderWidth: 2,
    borderColor: '#3F9E93',
    marginBottom: 18,
  },
  secondaryButtonText: {
    fontSize: 18,
    fontWeight: '600',
    color: '#3F9E93',
  },
  termText: {
    fontSize: 14,
    color: '#8E8E8E',
    textAlign: 'center',
  },
  linkText: {
    color: '#F7C050',
  },
  floatingButton: {
    paddingVertical: 6,
    paddingHorizontal: 14,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    borderWidth: 1,
    borderColor: '#3F9E93',
    position: 'absolute',
    // position: 'relative',
    top: Platform.OS === 'ios' ? 60 : 20,
    right: 20,
  },
  floatingButtonText: {
    fontSize: 16,
    color: '#3F9E93',
  },
});
