import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import HomeCarousel from '../components/HomeCarousel';
import HomeHeader from '../components/HomeHeader';
import HomeProductCard from '../components/HomeProductCard';
import SeeAllButton from '../components/SeeAllButton';
import HomeShopCard from '../components/HomeShopCard';

export default function Home() {
  const navigation = useNavigation();
  const [categoryList, setCategoryList] = useState([]);
  const [productList, setProductList] = useState([]);

  useEffect(() => {
    getCategory();
    getProducts();
  }, []);

  const getCategory = async () => {
    try {
      const res = await fetch('https://fakestoreapi.com/products/categories');
      const result = await res.json();
      if (result && result.length > 0) {
        setCategoryList([...result, ...result]);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getProducts = async () => {
    try {
      const res = await fetch('https://fakestoreapi.com/products?limit=9');
      const result = await res.json();
      if (result && result.length > 0) {
        setProductList(result);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      {/* Section 1 Header */}
      <HomeHeader navigation={navigation} />

      {/* Section 2 Carousel */}
      <HomeCarousel />

      {/* Section 3 Balance & Voucher */}
      <View style={styles.balanceContainer}>
        <View style={styles.balanceWrapper}>
          <View style={styles.fakeCoin}>
            <Text>S</Text>
          </View>
          <View>
            <Text style={styles.balanceText}>Rp0</Text>
            <Text style={{fontSize: 12, color: '#8E8E8E'}}>Surplus Pay</Text>
          </View>
        </View>

        <View style={styles.balanceWrapper}>
          <View style={styles.fakePaper}>
            <Text>S</Text>
          </View>
          <Text style={styles.balanceText}>5 voucher tersedia</Text>
        </View>

        <View style={styles.balanceSeparatorLine} />
      </View>

      {/* Category */}
      <View style={styles.categoryContainer}>
        <View style={styles.sectionTitleContainer}>
          <Text style={styles.sectionTitleText}>Kategori</Text>
        </View>
        <ScrollView
          horizontal
          contentContainerStyle={{paddingLeft: 20}}
          showsHorizontalScrollIndicator={false}>
          {categoryList?.map((cat, index) => {
            return (
              <TouchableOpacity
                key={`key-${index}`}
                style={{
                  backgroundColor: '#F1F1F1',
                  width: 88,
                  height: 88,
                  borderRadius: 8,
                  overflow: 'hidden',
                  marginRight: 10,
                  padding: 4,
                }}>
                <Text style={{fontSize: 16, fontWeight: '600'}}>{cat}</Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>

      {/* Badges */}
      <View
        style={{
          flexDirection: 'row',
          marginHorizontal: 20,
          borderRadius: 10,
          height: 88,
          overflow: 'hidden',
          marginBottom: 32,
        }}>
        <Image
          source={undefined}
          style={{height: '100%', width: '32%', backgroundColor: 'grey'}}
        />
        <View
          style={{
            marginHorizontal: 8,
            paddingVertical: 12,
            flex: 1,
            justifyContent: 'space-between',
          }}>
          <Text style={{fontSize: 14, color: '#000000'}}>
            Sssst ada yang bakal kebuka kalau kamu transaksi di Surplus
          </Text>
          <View
            id="progress-bar"
            style={{
              width: '100%',
              height: 4,
              borderRadius: 4,
              backgroundColor: '#F1F1F1',
            }}
          />
          <Text style={{fontSize: 10, color: '#4A4A4A'}}>
            Lakukan 1x transaksi untuk membuka badges
          </Text>
        </View>
      </View>

      {/* Di sekitar kamu */}
      <View
        style={[
          styles.sectionTitleContainer,
          {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          },
        ]}>
        <View>
          <Text style={styles.sectionTitleText}>Di sekitar kamu</Text>
          <Text style={styles.subTitleSectionText}>
            Yuk selamatkan barang di sekitarmu!
          </Text>
        </View>
        <SeeAllButton onPress={() => {}} />
      </View>
      <ScrollView
        horizontal
        contentContainerStyle={{paddingLeft: 20, marginBottom: 32}}
        showsHorizontalScrollIndicator={false}>
        {productList.map((item, index) => {
          return (
            <HomeProductCard
              key={`product1-${index}`}
              index={index}
              item={item}
            />
          );
        })}
        <HomeProductCard type="see-all" />
      </ScrollView>

      {/* Restoran Terdekat */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 20,
          marginBottom: 16,
        }}>
        <Text style={styles.sectionTitleText}>Restoran terdekat</Text>
        <SeeAllButton onPress={() => {}} />
      </View>
      <ScrollView
        horizontal
        contentContainerStyle={{paddingLeft: 20, marginBottom: 32}}
        showsHorizontalScrollIndicator={false}>
        {productList.map((item, index) => {
          return (
            <HomeShopCard key={`shop1-${index}`} item={item} index={index} />
          );
        })}
        <HomeShopCard type="see-all" />
      </ScrollView>

      {/* Makanan terselamatkan - Kerugian berhasil dicegah */}
      <View
        style={{
          height: 168,
          flex: 1,
          marginHorizontal: 20,
          borderWidth: 1,
          borderColor: '#F1F1F1',
          borderRadius: 8,
          marginBottom: 32,
          flexDirection: 'row',
          overflow: 'hidden',
        }}>
        <View style={{width: '50%', backgroundColor: 'grey', opacity: 0.1}} />
        <View
          style={{
            paddingHorizontal: 8,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: 24, fontWeight: '700', color: '#000000'}}>
            87.720
          </Text>
          <Text style={{fontSize: 12, marginBottom: 8}}>
            Makanan terselamatkan
          </Text>
          <Text style={{fontSize: 24, fontWeight: '700', color: '#000000'}}>
            Rp769.263.845
          </Text>
          <Text style={{fontSize: 12, marginBottom: 8}}>
            Kerugian berhasil dicegah
          </Text>
          <TouchableOpacity
            style={{
              borderWidth: 1,
              borderColor: '#3F9E93',
              borderRadius: 40,
              paddingVertical: 8,
              paddingHorizontal: 12,
            }}>
            <Text style={{fontSize: 14, color: '#3F9E93'}}>
              Lihat selengkapnya
            </Text>
          </TouchableOpacity>
        </View>
      </View>

      {/* Terima kasih */}
      <Text
        style={{
          fontSize: 14,
          color: '#4A4A4A',
          opacity: 0.7,
          textAlign: 'center',
          paddingBottom: 32,
        }}>
        Terima kasih sudah mendukung Surplus dalam{'\n'}menyelamatkan lingkungan
        di Indonesia
      </Text>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: '#FFFFFF',
  },
  balanceContainer: {
    backgroundColor: '#F1F1F1',
    flexDirection: 'row',
    marginHorizontal: 20,
    borderRadius: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 16,
    paddingHorizontal: 20,
    marginBottom: 32,
  },
  balanceWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  fakeCoin: {
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: 'orange',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 14,
  },
  fakePaper: {
    height: 16,
    width: 24,
    backgroundColor: 'orange',
    marginRight: 14,
    justifyContent: 'center',
    alignItems: 'center',
  },
  balanceText: {
    fontSize: 14,
    fontWeight: '600',
    color: '#3F9E93',
  },
  balanceSeparatorLine: {
    height: '100%',
    width: 2,
    backgroundColor: 'grey',
    position: 'absolute',
    left: '55%',
  },
  categoryContainer: {
    marginBottom: 32,
  },
  sectionTitleContainer: {
    paddingHorizontal: 20,
  },
  sectionTitleText: {
    fontSize: 18,
    fontWeight: '600',
    color: '#000000',
    marginBottom: 8,
  },
  subTitleSectionText: {
    fontSize: 14,
    color: '#4A4A4A',
    marginBottom: 8,
  },
});
