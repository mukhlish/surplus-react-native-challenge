# Surplus React Native Challenge

Show case your React Native skills!

## Marking and Plus Points
This list is just a guidance, if it's not possible to do the mandatory list, please note it into readme
1. (Mandatory) Use Open Public API for functional stuff in the random mocked first screen (i am using https://fakestoreapi.com/) (✅ Done)
2. (Mandatory) Login credential can be hardcoded, but better to also show case all possible scenarios (success login, failed login, success registering etc.) (✅❌ Half Done)
3. (Mandatory) App-Styles will be totally considered, make it as beatiful as it can be (✅ Done)
4. (Mandatory) Consider multiple phone layouts (the more merrier, but please tell us what phones you are using for developing the app) (✅ Done, 
    > **_NOTE:_** using real device - Xiaomi Redmi Note 8, android emulator - Pixel 6, and iOS simulator -  iPhone 11 Pro Max)
5. (Plus Point) ensure cross platform usablity (Android and iOS) (✅ Done, using simulator iPhone 11 Pro Max)
6. ~~(Plus Point) you can use other tech stacks if you want, e.g. Flutter~~ (❌ Not Done)
7. ~~(Plus Point) provide unit test and write the procedures in th readme.~~ (❌ Not Done)

# Steps and Procedure

1. Clone/copy this repository
2. Open terminal and after move to project directory, run command `npm install` to install required node modules

    > **_NOTE:_**  make sure `node` and `npm` already installed in your local computer

3. Connect your Android phone to your local machine (make sure "Developer Options" already unlocked) or you can use android emulator or iOS simulator (if using MacOS), then run this command in terminal

    ```
    // for android device
    npx react-native run-android

    // or for iOS
    npx react-native run-ios
    ```

    > **_NOTE:_**  in real device android, you have to tap "Accept" button when asked to install the app within 10 seconds or you have to re-run the command above

4. The app should be installed successfully

>I look forward to receiving various responses. Thank You
